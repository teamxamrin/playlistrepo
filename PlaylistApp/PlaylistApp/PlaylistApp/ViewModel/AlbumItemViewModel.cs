﻿using PlaylistApp.Model;
using PlaylistApp.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistApp.ViewModel
{
    public class AlbumItemViewModel : INotifyPropertyChanged
    { 
        public Album AlbumSelected { get; set; }

        public List<Song> _MySongs { get; set; }

        public List<Song> MySongs
        {
            get { return _MySongs; }
            set
            {
                _MySongs = value;
                OnPropertyChanged();
            }
        }

        public AlbumItemViewModel(Album album)
        {
            AlbumSelected = album;
            InitializeDataAsync();
        }

        private async Task InitializeDataAsync()
        {
            var songService = new SongService();
            MySongs = await songService.GetSongsAsync(AlbumSelected.Id);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
