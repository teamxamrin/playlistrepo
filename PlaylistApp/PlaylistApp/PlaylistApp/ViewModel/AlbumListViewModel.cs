﻿using PlaylistApp.Model;
using PlaylistApp.Service;
using PlaylistApp.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PlaylistApp.ViewModel
{
    public class AlbumListViewModel : INotifyPropertyChanged
    {
        private INavigation _navigation;

        public List<Album> _MyAlbum { get; set; }

        public List<Album> MyAlbum
        {
            get { return _MyAlbum; }
            set {
                _MyAlbum = value;
                OnPropertyChanged();
            }
        }

        public AlbumListViewModel(INavigation navigation)
        {
            _navigation = navigation;
            InitializeDataAsync();
        }

        private async Task InitializeDataAsync()
        {
            var albumService = new AlbumService();
            MyAlbum = await albumService.GetAlbumsAsync();
        }

        public Album SelectedAlbum
        {
            set
            {
                if (value != null)
                {
                    var viewModel = new AlbumItemViewModel(value);
                    var page = new AlbumItemPage(viewModel);
                    _navigation.PushAsync(page);
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
