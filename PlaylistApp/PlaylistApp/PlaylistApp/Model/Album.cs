﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistApp.Model
{
    public class Album
    {
        [JsonProperty("Id")]
        public Guid Id { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("Name")]
        public string ArtistNme { get; set; }

    }
}
