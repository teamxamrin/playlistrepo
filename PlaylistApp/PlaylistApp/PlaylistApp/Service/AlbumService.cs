﻿using PlaylistApp.Model;
using Plugin.RestClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistApp.Service
{
    public class AlbumService
    {
        public async Task<List<Album>> GetAlbumsAsync()
        {
            RestClient<Album> restClient = new RestClient<Album>("http://testplaylist.azurewebsites.net/api/album");
            var albums = await restClient.GetAsync();

            return albums;
        }
    }
}
