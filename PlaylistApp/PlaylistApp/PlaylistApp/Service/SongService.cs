﻿using PlaylistApp.Model;
using Plugin.RestClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistApp.Service
{
    public class SongService
    {
        public async Task<List<Song>> GetSongsAsync(Guid idAlbum)
        {
            RestClient<Song> restClient = new RestClient<Song>("http://testplaylist.azurewebsites.net/api/song?idAlbum=" + idAlbum);
            var songs = await restClient.GetAsync();

            return songs;
        }
    }
}
