﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PlaylistApp.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AlbumItemPage : ContentPage
    {

        public AlbumItemPage(ViewModel.AlbumItemViewModel viewModel)
        {
            Title = viewModel.AlbumSelected.Title + " - " + viewModel.AlbumSelected.ArtistNme;
            InitializeComponent();
            BindingContext = viewModel;
        }
    }
}