﻿using PlaylistApp.Model;
using PlaylistApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PlaylistApp.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AlbumListPage : ContentPage
    {
        public AlbumListPage()
        {
            InitializeComponent();
            BindingContext = new AlbumListViewModel(this.Navigation);

            AlbumListView.ItemSelected += (s, e) => {
                AlbumListView.SelectedItem = null;
            };
        }
    }
}